from uiautomator import device as d
import os
import time
from order.buy import buy_order
from order.buy import buy_order_limit
from order.buy import buy_order_market
from order.sell import sell_order
from order.sell import sell_order_limit
from order.sell import sell_order_market
from Init.init import *


def getCommission():
    if d(description="暂无委托单").exists:
        print("No Record")
    else:
        print("股票/时间             成交/委托           状态              操作")

        try:
            header = d(index="1")[2]
            index = 1
            while header.exists == True:
                ele1 = header.child(index="1")
                stockName = ele1.child(index="0").description
                time = ele1.child(index="1").description
                ele2 = header.child(index="2")
                number = ele2.child(index="0").description
                price = ele2.child(index="1").description
                ele3 = header.child(index="3")
                status = ele3.child(index="0").description
                type = ele3.child(index="1").description
                ele4 = header.child(index="4")
                operate = ele4.child(index="0").description
                print(stockName+"/"+time+"     "+number+"/"+price+"     "+status+"/"+type+"      "+operate)
                index = index + 1
                header = header.down(index=str(index))
        except Exception:
            print("end")

#得到委托单信息
def getCommissionA():
    if(isDeviceConnect() == 1):
        lightScreen()
        while(isStockPosition() == 0):
            launchActivity()
        d(text="委托单").click.wait()
        while d(text="委托单").exists == False:
            print("waiting....")
        d.swipe(300, 1500, 785, 1776, steps=10)
        time.sleep(3)
        d.dump("view.xml")
        while d(description="状态").exists == False:
            print("waiting....")
        getCommission()
    else:
        print("wait for device")



def getDealRecords():
    print("股票         成交价/数量         手续费(参考)       时间")
    d.dump("view.xml")
    try:
        h = d(index="6")[0]
        count = 2
        header = d(index="1")[6]
        while (header.exists):
            #print(header.childCount)
            stockName = header.child(index="0").description
            stockPrice = header.child(index="1").description
            extraCost = header.child(index="2").description
            time = header.child(index="3").child(index="0").description
            print(stockName+"       "+stockPrice+"         "+extraCost+"        "+time)
            header = header.down(index=str(count))
            count = count + 1
    except Exception:
        print("End")
    finally:
        pass

#得到交易记录
def getDealRecordsA():
    if(isDeviceConnect() == 1):
        lightScreen()
        while(isStockPosition() == 0):
            launchActivity()
        d(text="交易记录").click.wait()
        while d(description="今日交易").exists == False:
            print("waiting...")
        getDealRecords()
    else:
        print("wait for device")






#def main():
    #买入
    #buy_order("SH513600",100)
    #buy_order("SH502004",100)
    #卖出
    #sell_order("SH513600",100)
    #sell_order("SH502004",100)



#main()
#getDealRecordsA()
#getCommissionA()
#d.dump("view.xml")




