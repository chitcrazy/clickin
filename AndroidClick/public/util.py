import time
from uiautomator import device as d

#键盘
keycode = {"0":7,"1":8,"2":9,"3":10,"4":11,"5":12,"6":13,"7":14,"8":15,"9":16,".":56}

def fnn(stockName):
    d(description="股票").click.wait()
    d(text="输入股票名称/代码").set_text(stockName)
    item = d(resourceId="com.xueqiu.android:id/text")[0]
    posi = item.text.find(stockName)
    print(posi)
    print(item.text)
    while posi == -1:
        d(text=stockName).clear_text()
        d(text="输入股票名称/代码").set_text(stockName)
        time.sleep(1)
        item = d(resourceId="com.xueqiu.android:id/text")[0]
        while  item.exists == False:
            print("wait...")
        posi = item.text.find(stockName)
        print(posi)
        print(item.text)
    d(resourceId="com.xueqiu.android:id/text")[0].click.wait()


def isOrderView():
    while d(description="买 入").exists == False:
        if d(text="持有市值").exists == True:
            d(text="下单").click.wait()


#判断下单是否成功
def isCommitSuccess():
    flag = 0
    if d(description="交易委托单已发送").exists == True:
        flag = 1
    return flag

#如果要求输入交易密码，则输入，否则略过
def passwd():
    password = "645852"
    length = len(password)
    if d(text="请输入交易密码").exists == True:
        d(className="android.widget.EditText").click.wait()
        for j in range(0,length):
            d.press(keycode[password[j]])
        d(text="确定").click.wait()