from pymongo import MongoClient
import json
from datetime import datetime

__author__ = 'yuyifei'


class MongodbOperation:
    client = None
    stock_data_db = None
    stock_collection = None
    account_collection = None

    def __init__(self):
        with open('login.json') as f:
            login_data = json.load(f)

        client = MongoClient(host=login_data['host'])

        client.stock_data.authenticate(login_data['user'], login_data['other'])

        self.client = client
        self.stock_data_db = client.stock_data
        self.stock_collection = client.stock_data.stock_info
        self.account_collection = client.stock_data.stock_account


    def createAccount(self, id, name, account):
        account_dist = {}
        account_dist.setdefault('id', id)
        account_dist.setdefault('name', name)
        account_dist.setdefault('acconut', account)

        inserted_id = self.account_collection.insert_one(account_dist)

        if not inserted_id == None:
            print ("[d] insert account success ... " )
            print (account_dist)

    @classmethod
    def jugde_time(cls):
        now = datetime.now()

        # if today is weekday, return False.
        is_weekday = now.today().weekday()
        if  is_weekday == 5 or is_weekday == 6:
            return False

        hour = int(now.hour)
        minute = int(now.minute)

        big_int = hour * 100 + minute

        # 9:30 - 11:30
        if 930 < big_int and big_int < 1130:
            return True

        # 13:00 - 14:56
        if 1300 < big_int and big_int < 1456:
            return True

        return False


    @classmethod
    def get_stock_price(cls, stock_code):
        return 1000


    def control_center(self, stock_code, stock_num):
        # judge time.
        if not MongodbOperation.jugde_time():
            return 1

        # judge money.
        # current_money = self.account_collection.find_one({"id" : "sa12226385"})['acconut']
        # stock_price = MongodbOperation.get_stock_price(stock_code)
        # needed_money =  stock_price * stock_num
        #
        # if int(current_money) < needed_money:
        #     return 2

        return 0

    def record_stock_exchange(self, exchange_json_data):
        # record exchanged stock.
        stock_info = self.stock_collection

        inserted_id = stock_info.insert_one(exchange_json_data)
        if not inserted_id == None:
            print ("insert data success")

    def display_exchange_data(self):
        stock_info = self.stock_collection

        for info in stock_info.find():
            #print (json.dumps(info))
            print (info)




def record_stock_exchange(stock_code, stock_price, stock_num):
    db_operation = MongodbOperation()

    account = db_operation.account_collection.find_one({"id" : "sa12226385"})
    # deduct money.
    current_money = account['account']
    spend_money = stock_price * stock_num
    account['account'] = str(current_money - spend_money)

    # record exchanged stock.
    stock_info = db_operation.stock_collection
    info = {}
    info.setdefault('stock_code', stock_code)
    info.setdefault('stock_num', stock_num)
    info.setdefault('stock_price', stock_price)
    info.setdefault('sum_money', spend_money)
    # record it to db.
    stock_info.insert_one(info)


def main():
    db_operation = MongodbOperation()

    # create a yuyifei accunt.
    # id = 'sa12226385'
    # name = 'yuyifei'
    # account = '10000'
    # db_operation.createAccount(id, name, account)

    bug_flag = False
    stock_code = '0001'
    stock_num = 20
    result = db_operation.control_center(stock_code, stock_num)
    if result == 0:
        bug_flag = True
        print ("[d] buy stock ...")

    if result == 1:
        print ("[d] time is incorrect.")

    if result == 2:
        print ("[d] money is not enough.")



if __name__ == '__main__':
    main()
