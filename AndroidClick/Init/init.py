from uiautomator import device as d
import os

#获取持仓记录
def getStockPositionDate():
    #d.dump("view.xml")
    total = d(resourceId="com.xueqiu.android:id/asset_balance").text
    proAndCons = d(resourceId="com.xueqiu.android:id/income_data").text
    print("平安证券-总资产:"+total+"     "+"今日参考盈亏:"+proAndCons)

    a=[]
    print("持有市值       浮动盈亏        可用     可取")
    paramNumber = len(d(resourceId="com.xueqiu.android:id/value"))
    for j in range(0,paramNumber):
        a.append(d(resourceId="com.xueqiu.android:id/value")[j].text)
    print(a[0]+"         "+a[1]+"        "+a[2]+"         "+a[3])

    stockNumber = len(d(resourceId="com.xueqiu.android:id/column_1_row_1"))
    print("股票/市值           持仓/可卖           现价/成本          盈亏")
    for i in range(0,stockNumber):
        stockName = d(resourceId="com.xueqiu.android:id/column_1_row_1")[i].text
        stockPrice = d(resourceId="com.xueqiu.android:id/column_1_row_2")[i].text
        position = d(resourceId="com.xueqiu.android:id/column_2_row_1")[i].text
        seller = d(resourceId="com.xueqiu.android:id/column_2_row_2")[i].text
        currentItemPrice = d(resourceId="com.xueqiu.android:id/column_3_row_1")[i].text
        cost = d(resourceId="com.xueqiu.android:id/column_3_row_2")[i].text
        pros = d(resourceId="com.xueqiu.android:id/column_4_row_1")[i].text
        cons = d(resourceId="com.xueqiu.android:id/column_4_row_2")[i].text
        print(stockName+"/"+stockPrice+"     "+position+"/"+seller+"       "+currentItemPrice+"/"+cost+"      "+pros+"/"+cons)

#判断手机是否处于连接状态,成功返回1,不成功返回0
def isDeviceConnect():
    commandData = os.popen("adb devices")
    commandData.readline()
    data = commandData.readline()
    if len(data.strip()) == 0:
        return 0
    else:
        return 1

#屏幕点亮并划屏
def lightScreen():
    d.screen.on()
    d.swipe(300, 1500, 785, 1776, steps=10)

#判断是否处于持仓界面,成功返回1,不成功返回0
def isStockPosition():
    flag = 0
    try:
        d(text="持有市值").click()
        d(text="浮动盈亏").click()
        d(text="可用").click()
        d(text="可取").click()
        flag = 1
    except Exception:
        flag = 0
    return flag

#启动软件,点击交易进入持仓界面
def launchActivity():
    os.system('adb shell am start -n com.xueqiu.android/com.xueqiu.android.view.WelcomeActivityAlias')
    d(text="交易").click()

#判断下单数据是否合法
#param:stockName股票名称，number股数
def isDataLegal(stockName,number):
    return 1