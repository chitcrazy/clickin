from public.util import fnn
from public.util import isOrderView
from public.util import isCommitSuccess
from public.util import passwd
from uiautomator import device as d
import time
from Init.init import *

#键盘
keycode = {"0":7,"1":8,"2":9,"3":10,"4":11,"5":12,"6":13,"7":14,"8":15,"9":16,".":56}

#下单买入
#param:stockName股票名称，number股数
def buy_order(stockName,number):
    if(isDeviceConnect() == 1):
        lightScreen()
        while(isStockPosition() == 0):
            launchActivity()
        getStockPositionDate()

        if isDataLegal("SH513600",200) == 1:
            print("开始进行下单操作")
            d(text="下单").click.wait()
            #when the net has problem,call isOrderView()
            isOrderView()
            try:
                d(description="买入").click.wait()
                print(1)
            except Exception:
                d(description="买入").click.wait()
                print(2)
            fnn(stockName)

            #when the net has problem
            stockNameQuest = d(description="股票").down(className="android.widget.EditText").description
            print("stockNameQuest:"+stockNameQuest)
            print(stockName[2:len(stockName)])
            position = stockNameQuest.find(stockName[2:len(stockName)])
            print("stockName:"+str(position))
            while position == -1:
                fnn(stockName)
                stockNameQuest = d(description="股票").down(className="android.widget.EditText").description
                print("stockNameQuest:"+stockNameQuest)
                print(stockName[2:len(stockName)])
                position = stockNameQuest.find(stockName[2:len(stockName)])
                print("stockName:"+str(position))


            gushu = d(description="数量").down(className="android.widget.EditText")
            if gushu.description != "":
                print(gushu.description)
                print(len(gushu.description))
                p = len(gushu.description)
                gushu.click.wait()
                for j in range(0,p):
                    d.press(0x43)
                d.press(0x4)
            count = number / 100
            cou = int(count)
            for i in range(0,cou):
                d(className="android.widget.Button")[4].click()

            try:
                buy_order_market()
                d(description="查看委托单").click.wait()
            except Exception:
                passwd()
                time.sleep(2)
                flag = isCommitSuccess()
                #when the net has problem
                while flag == 0:
                    try:
                        d(description="买 入").click.wait()
                    except Exception:
                        passwd()
                        time.sleep(2)
                        flag = isCommitSuccess()
                        if flag == 1:
                            break
                        else:
                            continue
                    d(text="确定").click.wait()
                    passwd()
                    time.sleep(2)
                    flag = isCommitSuccess()
                    print("flag:"+str(flag))
                d(description="查看委托单").click.wait()

        else:
            print("下单数据不合法")

    else:
        print("wait for device")




#市价委托买入
def buy_order_market():
    d(index="6").click()
    d(description="市价委托").click.wait()
    d(description="买 入").click.wait()
    d(text="确定").click.wait()


#限价委托买入,用于测试
def buy_order_limit():
    header = d(index="4")
    s1 = header.child(index="0").description
    s2 = header.child(index="1").description
    s3 = header.child(index="2").description
    s11 = s1.split("(")
    new = s11[0][2:len(s11[0])]
    high = s2[2:len(s2)]
    low = s3[2:len(s3)]
    d(index="8").click.wait()
    length = len(new)
    length1 = len(low)
    for i in range(0,length):
        d.press(0x43)
    for j in range(0,length1):
        d.press(keycode[low[j]])
    d.press(0x4)
    d(description="买 入").click.wait()
    d.dump("view.xml")
    d(text="确定").click.wait()