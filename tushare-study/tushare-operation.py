import tushare as ts
import time

__author__ = 'yuyifei'

def realtime_monitor(code_name):
    while (True):
        df = ts.get_realtime_quotes('000581') #Single stock symbol
        print (df[['code','name','price','bid','ask','volume','amount','time']])
        time.sleep(2)

def main():
    """
    http://tushare.waditu.com/trading.html#id6
    https://pypi.python.org/pypi/tushare/
    """

    print (ts.__version__)

    realtime_monitor('600355')

if __name__ == '__main__':
    main()