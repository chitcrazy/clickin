# coding=utf-8
# MODULE: utils.py

# Author: Yu Guo
# Author: Hao Chen
# Author: Yifei Yu

# Changelog:
#     [20151120] guoyu: This module is for auxiliary functions

import datetime
import time
import os
import sys
import winsound
import random
import platform


def get_platform():
    return platform.architecture()

def play_succ_sound(i):
    """play sound for notifying"""
    if platform.system() == "Windows":
        import winsound
        for i in range(0, i):
            winsound.PlaySound('tick.wav', winsound.SND_FILENAME)
        #winsound.PlaySound('SystemExit', winsound.SND_ALIAS)

def play_alert_sound(i):
    """play sound for notifying"""
    if platform.system() == "Windows":
        import winsound
        for i in range(0, i):
            winsound.PlaySound('alert.wav', winsound.SND_FILENAME)

def get_random_number():
    rint = random.randrange(0, 10000000)
    return rint

def get_random_real():
    rreal = random.random()
    return rreal
    
def get_current_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def timestamp_now() -> int:
    return int(time.time() * 1000)

def paren(text):
    pstack = 0
    start = 0
    end = len(text)
    for i, c in enumerate(text):
        if c == '(':
            if pstack == 0:
                start = i
            pstack += 1
        elif c == ')':
            pstack -= 1
            if pstack == 0:
                end = i
                break
    return text[start+1:end]

# Test
if __name__ == '__main__':
    pass

