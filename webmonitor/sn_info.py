#!/usr/bin/python3
# coding=utf-8

'''
访问网址: http://hq.sinajs.cn/?_=0.7073744484223425&list=sh600795
返回结果:
var hq_str_sh600795=var hq_str_sh600795="国电电力,4.26,4.27,4.25,4.27,4.23,4.25,4.26,
144627385,613934057,
15100,4.25,2139004,4.24,4127843,4.23,3152500,4.22,3967400,4.21,
2942561,4.26,3844695,4.27,3063121,4.28,3241500,4.29,4248252,4.30,
2015-11-20,15:04:05,00";

这些值的具体含义如下:

0:      国电电力,           股票名称
1:      4.26,               今日开盘价
2:      4.27,               昨日收盘价
3:      4.25,               当前价格
4:      4.27,               今日最高价
5:      4.23,               今日最低价
6:      4.25,               竞买价，即“买一”报价
7:      4.26,               竞卖价，即“卖一”报价
8:      144627385,          成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
9:      613934057,          成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
10:     15100, 4.25,   买1
12:     2139004, 4.24, 买2
14:     4127843, 4.23, 买3
16:     3152500, 4.22, 买4
18:     3967400, 4.21, 买5 
20:     2942561, 4.26, 卖1
22:     3844695, 4.27, 卖2
24:     3063121, 4.28, 卖3
26:     3241500, 4.29, 卖4
28:     4248252, 4.30, 卖5
30:     2015-11-20,15:04:05,00"; 时间

'''

import urllib.request

def get_price(code):
    try:
        url = 'http://hq.sinajs.cn/?list=%s' % code
        req = urllib.request.Request(url)
        content = urllib.request.urlopen(req).read()
        str = content.decode('gbk')
        data = str.split('"')[1].split(',')
        name = "%-6s" % data[0]
        price_current = "%-6s" % float(data[3])
        change_percent = ( float(data[3]) - float(data[2]) )*100 / float(data[2])
        change_percent = "%-6s" % round (change_percent, 2)
        print("股票名称:{0} 涨跌幅:{1} 最新价:{2}".format(name, change_percent, price_current) )
    except:
        pass
    
def get_all_price(code_list):
    for code in code_list:
        get_price(code)

code_list = ['sz300036', 'sz000977', 'sh600718', 'sh600452', 'sh600489']
get_all_price(code_list)
