import urllib
import urllib.request
import urllib.error
import http.cookiejar
import datetime
from time import sleep
import json
import time
import os
import sys
import socket

import utils
import config
import debug
from debug import log

class Monitor:

    _user = ""
    _passwd = ""
    _base_url = ""
    _headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'}
    _cj = None
    _opener = None
    _url_login = ""
    _website_name = "<未设定>"

    def __init__(self):
        super().__init__()
        self._cj = http.cookiejar.CookieJar()
        self._opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self._cj))
        urllib.request.install_opener(self._opener)

    def login_post_data(self):
        """abstract method"""
        return {}

    def login(self):
        data = self.login_post_data();
        post_data = urllib.parse.urlencode(data).encode(encoding='UTF8')
        try:
            req = urllib.request.Request(self._url_login, post_data, headers = self._headers)
            response = self._opener.open(req, post_data)

            print(utils.get_current_timestamp(), end=":")
            if response.status != 200:
                log(debug.FATAL, self._website_name +"登录失败")
                log(debug.INFO, "user=%s passwd=%s" % (self._user, self._passwd))
                return False
        except urllib.error.URLError:
            log(debug.FATAL, "URL无法访问" + "URL=%s" % self._url_login)
            log(debug.FATAL, self._website_name +"登录失败")
            return False
        else:
            log(debug.INFO, self._website_name + "登录成功")
            sleep(0.1)
            return True

    def get_zuhe_trade_url(self, zuhe_id) -> str:
        """abstract method"""
        return ""

    def zuhe_id_is_valid(self, zuhe_id) -> bool:
        """abstract method"""
        return False

    def data_is_new(self, json_data: dict, old_data: dict) -> bool:
        """abstract method"""
        return False

    def get_stock_url(self, stock_id):
        raise NotImplementedError

    def extract_stock_info(self, stock_id, raw_data):
        raise NotImplementedError

    def get_stock_info(self, stock_id):
        stock_info = {}
        try:
            raw_data = self._retrieve_stock_data(stock_id)
            if not raw_data:
                log(debug.ERROR, "读取数据为空")
            json_data =json.loads(raw_data)
            stock_info = self.extract_stock_info(stock_id, json_data)
        except ValueError:
             log(debug.FATAL, "键值错误")
             log(debug.INFO, str(raw_data))
             self.on_check_error("键值错误")
        except KeyError as e:
             log(debug.FATAL, "键名错误: %s" % str(e))
             log(debug.INFO, str(raw_data))
             self.on_check_error("键名错误")
        log(debug.DEBUG, str(stock_info))
        return stock_info

    def _retrieve_stock_data(self, stock_id) -> str:
        stock_url = ""
        try:
            stock_url = self.get_stock_url(stock_id)
            req = urllib.request.Request(stock_url, headers=self._headers)
            log(debug.DEBUG, "Read URL: " + stock_url)
            raw_data = urllib.request.urlopen(req, None, timeout=3).read().decode('utf-8')
            log(debug.DEBUG, "Extract Data")
            return  raw_data

        except socket.gaierror:
            log(debug.FATAL, "地址解析错误, 请检查网络!")
            self.on_check_error("地址解析错误, 请检查网络!")

        except socket.timeout:
            self.on_check_error("TIMEOUT")

        except urllib.error.HTTPError as e:
            log(debug.ERROR, "HTTPError, 可能是请求被驳回")
            log(debug.DEBUG, "URL:" + stock_url)
            self.on_check_error("HTTP请求出错")

        except urllib.error.URLError as e:
            log(debug.ERROR, "URLError")
            self.on_check_error("URL出错")

        return ""

    def on_check_error(self, msg):
        print("<%s>" % msg, end=" ")
        sys.stdout.flush()

    def on_check_idle(self):
        print(".", end=" ")
        sys.stdout.flush()

    def on_check_new_trade(self, json_data, msg):
        """abstract method"""
        pass

    def _retrieve_zuhe_data(self, zuhe_id) -> str:
        json_raw_data = ""
        zuhe_url = ""
        try:
            zuhe_url = self.get_zuhe_trade_url(zuhe_id)
            req = urllib.request.Request(zuhe_url, headers=self._headers)
            log(debug.DEBUG, "sleeping ")
            sleep(2)
            log(debug.DEBUG, "read url" + zuhe_url)
            json_raw_data = urllib.request.urlopen(req, None, timeout=3).read().decode('utf-8')
            log(debug.DEBUG, "extract data")
            return json_raw_data

        except socket.gaierror:
            log(debug.FATAL, "地址解析错误, 请检查网络!")
            self.on_check_error("地址解析错误, 请检查网络!")

        except socket.timeout:
            self.on_check_error("TIMEOUT")

        except urllib.error.HTTPError as e:
            log(debug.ERROR, "HTTPError, 可能是请求被驳回")
            self.on_check_error("HTTP请求出错")

        except urllib.error.URLError as e:
            log(debug.ERROR, "URLError")
            self.on_check_error("URL出错")
        except Exception as e:
            log(debug.ERROR, "未知错误" + str(e))
            log(debug.DEBUG, zuhe_url)
            self.on_check_error("未知错误")
            log(debug.DEBUG, json_raw_data)
            raise e

        return {}

    _old_data = {}

    def check(self, zuhe_id):
        if not self.zuhe_id_is_valid(zuhe_id):
            raise Exception("[E] 组合ID不合法 %s" % zuhe_id)

        log(debug.INFO, "[开始监控股票组合] [" + self._website_name+"]")
        new_data = ""
        while 1:
            try:
                new_data = self._retrieve_zuhe_data(zuhe_id)
                if (not new_data):
                    continue
                else:
                    is_new = self.data_is_new(new_data, self._old_data)
                log(debug.DEBUG, "New data arrived ? "+ str(is_new))
                if is_new:
                    self.on_check_new_trade(new_data, "")
                    self._old_data = new_data
                    utils.play_succ_sound(1)
                else:
                    self.on_check_idle()

            # except ValueError:
            #     log(debug.FATAL, "键值错误")
            #     log(debug.INFO, str(new_data))
            #     log(debug.INFO, str(self._old_data))
            #     self.on_check_error("键值错误")
            #
            # except KeyError:
            #     log(debug.FATAL, "键名错误")
            #     log(debug.INFO, str(new_data))
            #     log(debug.INFO, str(self._old_data))
            #     self.on_check_error("键名错误")

            except KeyboardInterrupt:
                log(debug.FATAL, "<用户 Ctrl-C 中断>")
                break

        log(debug.INFO, "[结束监控股票组合]")
        return

    _zuhe_list_old_data = {}
    _zuhe_list_url_cache = {}

    def check_zuhe_list(self, zuhe_id_list):

        for z in zuhe_id_list:
            if not self.zuhe_id_is_valid(z):
                raise Exception("[E] 组合ID不合法 %s" % z)
            if z in self._zuhe_list_url_cache:
                continue;
            self._zuhe_list_url_cache[z] = self.get_zuhe_trade_url(z)

        log(debug.INFO, "[开始监控股票组合列表]")

        new_data = ""
        while 1:
            try:
                for z in zuhe_id_list:
                    new_data = self._retrieve_zuhe_data(z)
                    if (not new_data):
                        continue
                    if not (z in self._zuhe_list_old_data):
                        old_data = ""
                    else:
                        old_data = self._zuhe_list_old_data[z]
                    is_new = self.data_is_new(new_data, old_data)
                    if is_new:
                        log(debug.DEBUG, str(new_data))
                        self.on_check_new_trade(new_data, "")
                        self._zuhe_list_old_data[z] = new_data
                        utils.play_succ_sound(1)
                    else:
                        self.on_check_idle()

            except ValueError:
                 log(debug.FATAL, "键值错误")
                 log(debug.INFO, str(new_data))
                 log(debug.INFO, str(old_data))
                 self.on_check_error("键值错误")

            except KeyError as e:
                 log(debug.FATAL, "键名错误: %s" % str(e))
                 log(debug.INFO, str(new_data))
                 log(debug.INFO, str(old_data))
                 self.on_check_error("键名错误")

            except KeyboardInterrupt:
                log(debug.FATAL, "<用户 Ctrl-C 中断>")
                break

        log(debug.INFO, "[结束监控股票组合]")
        return

class XueqiuMonitor(Monitor):

    _base_url = "http://xueqiu.com"

    def __init__(self, user, passwd):
        super().__init__()
        self._user = user
        self._passwd = passwd
        self._url_login = 'http://xueqiu.com/user/login'
        self._website_name = "雪球网"

    def login_post_data(self):
        return {'username':self._user, 'password': self._passwd}

    def get_stock_url(self, stock_id):
        url_base= "http://xueqiu.com/v4/stock/quote.json"
        stock_url = url_base + ("?code=" + stock_id) + ("&_=" + str(utils.timestamp_now()))
        return stock_url


    def get_zuhe_trade_url(self, zuhe_id):
        rec_count = 40
        url_base = "http://xueqiu.com/cubes/rebalancing/history.json"
        zuhe_url = url_base + ("?cube_symbol=" + zuhe_id) + ("&count=" + str(rec_count)) \
                   + "&page=1"
        return zuhe_url

    def zuhe_id_is_valid(self, zuhe_id) -> bool:
        if zuhe_id[0:2] == "ZH":
            return True
        else:
            return False

    def extract_stock_info(self, stock_id, json_data):
        info = {}
        if not json_data:
            return info
        data = json_data[stock_id]
        info['id'] = data['symbol']
        info['name'] = data['name']
        info['price'] = data['current']
        info['change_rate'] = data['percentage']
        return info

    def data_is_new(self, new_raw_data, old_raw_data) -> bool:
        new_json_data = json.loads(new_raw_data)
        new_trade = new_json_data['list'][0]
        if new_trade['status'] != "success":
            return False
        if not old_raw_data:
            return True

        old_json_data = json.loads(old_raw_data)
        old_trade = old_json_data['list'][0]

        new_trade_timestamp = new_trade['updated_at']
        old_trade_timestamp = old_trade['updated_at']
        log(debug.DEBUG, "old time: %s , new time %s " %(str(old_trade_timestamp), str(new_trade_timestamp)))
        if new_trade_timestamp != old_trade_timestamp :
            return True
        else:
            return False

    def print_trade_record(self, record_data):
        """ print the recent trade record to the console from the stock data grabbed """
        for i in range(len(record_data)):
            print('时间',end=':')
            trade_time = datetime.datetime.fromtimestamp(record_data[i]['updated_at'] / 1000.0)
            print(trade_time.strftime("%Y-%m-%d %H:%M:%S"), end=' | ')
            print('名称',end=':')
            print("%s (%s)" % (record_data[i]['stock_name'], record_data[i]['stock_symbol'],),
                    end = ' ')
            print(" 持仓变化 %s  --> %s " % (record_data[i]['prev_weight'],
                    record_data[i]['target_weight']))
            print("              价格 %s | " % record_data[i]['price'])

    def on_check_new_trade(self, raw_data, msg):
        new_json_data = json.loads(raw_data)
        records = new_json_data['list'][0]['rebalancing_histories']
        print(" ")
        print(" ----------------------- ")
        self.print_trade_record(records)
        print(" ----------------------- ")

class EastmoneyMonitor(Monitor):

    _base_url = "http://eastmoney.com"

    def __init__(self, user, passwd):
        super().__init__()
        self._user = user
        self._passwd = passwd
        self._url_login = 'http://passport.eastmoney.com/ajax/lg.aspx'
        self._website_name = "东方财富网"

    def login_post_data(self):
        r = str(utils.get_random_real())
        data = {'vcode': '', 'u':self._user, 'p':self._passwd, 'r':r}  #登陆用户名和密码
        return data

    def get_zuhe_trade_url(self, zuhe_id):
        rec_count = 20
        url_prefix = "http://moniapi.eastmoney.com/webapi/json.aspx?type=deals_all"
        rand = utils.get_random_number()
        zuhe_url = url_prefix + ("&zh=" + str(zuhe_id)) + "&recIdx=0" \
                + ("&recCnt=%s" % rec_count) \
                + ("&js=zuheinfo%d((x))&callback=zuheinfo%d&_=1447937423471' " % (rand, rand))
        return zuhe_url

    def zuhe_id_is_valid(self, zuhe_id) -> bool:
        if str(zuhe_id).isdigit():
            return True
        return False

    def paren(self, text:str) -> str:
        pstack = 0
        start = 0
        end = len(text)
        for i, c in enumerate(text):
            if c == '(':
                if pstack == 0:
                    start = i
                pstack += 1
            elif c == ')':
                pstack -= 1
                if pstack == 0:
                    end = i
                    break
        return text[start+1:end]

    def data_is_new(self, new_raw_data, old_raw_data) -> bool:
        new_data = self.paren(new_raw_data)
        new_json_data = json.loads(new_data)
        if not new_json_data['success']:
            self.on_check_error("返回fail")
            log(debug.INFO, "服务器返回值为fail")
            return False
        if not old_raw_data:
            return True
        old_data = self.paren(old_raw_data)
        old_json_data = json.loads(old_data)
        new_trade = new_json_data['data'][0]
        old_trade = old_json_data['data'][0]

        new_trade_timestamp = new_trade['cjsj']
        old_trade_timestamp = old_trade['cjsj']
        log(debug.DEBUG, "old time: %s , new time %s " %(str(old_trade_timestamp), str(new_trade_timestamp)))
        if new_trade_timestamp != old_trade_timestamp :
            return True
        else:
            return False
# END of class EastmoneyMonitor(Monitor)

    def print_trade_record(self, record):
        """ print the recent trade record to the console from the stock data grabbed """
        print('成交日期', end=':')
        print(record['cjrq'], end=' ')
        print(record['cjsj'])
        print(record['mmbz'], end=' ')
        print('股票代码', end=':')
        print(record['__code'], end='     ')
        print(record['__name'], end='    ')
        print(record['cjjg'], end='    数量: ')
        print(record['cjsl'])

    def on_check_new_trade(self, raw_data, msg):
        new_data = self.paren(raw_data)
        new_json_data = json.loads(new_data)
        record = new_json_data['data'][0]
        print(" ")
        print(" ----------------------- ")
        self.print_trade_record(record)
        print(" ----------------------- ")

def parse_cmd():
    if len(sys.argv) < 2:
        print('Usage: ' + sys.argv[0] + '<ZH000001>')
        sys.exit(0)
    if len(sys.argv) == 2:
        zuhe_id = sys.argv[1]
    if len(sys.argv) > 2:
        zuhe_id_list = sys.argv[1:]

if __name__ == '__main__':
    print("run test.py for testing")