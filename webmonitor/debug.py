import utils
import config

# TODO: if debug_on: print to console
#        else: write into log files, or database
debug_on = True

# debug level :
#       0: None
#       1: FATAL
#       3: ERROR
#       5: Warning
#       7: Information
#       9: debug

FATAL = 0
ERROR = 3
WARN = 5
INFO = 7
DEBUG = 9

msg_prefix = {0: "[F]", 3:"[E] ", 5: "[W]", 7:"[I]", 9:"[D]"}

def log(level, msg, end="\n"):

    if level <= config.devel_level:
        print(msg_prefix[level], end=" ")
        print(msg, end)
    else:
        pass # write into a log file
    return
