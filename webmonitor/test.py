import monitor

def test_xq_check():
    m = monitor.XueqiuMonitor('macdata@qq.com', 'guoyu2guoyu22222222')
    r = m.login()
    if r:
        m.check('ZH185989')

def test_xq_check_list():
    m = monitor.XueqiuMonitor('macdata@qq.com', 'guoyu2guoyu22222222')
    r = m.login()
    if r:
        m.check_zuhe_list(['ZH185989', 'ZH021002'])

def test_em_check():
    e =  monitor.EastmoneyMonitor('gygy1001', 'guoyu2guoyu')
    r = e.login()
    if r:
        e.check('1678608')

def test_em_check_list():
    e =  monitor.EastmoneyMonitor('gygy1001', 'guoyu2guoyu')
    r = e.login()
    if r:
        e.check_zuhe_list(['1678608', '22714'])

def test_xq_sinfo():
    m = monitor.XueqiuMonitor('macdata@qq.com', 'guoyu2guoyu22222222')
    r = m.login()
    if r:
        m.get_stock_info('SH600466')

test_xq_sinfo()
